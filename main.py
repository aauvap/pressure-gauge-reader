import numpy as np
import cv2
import scipy.io
import time
from pathlib import Path

from gread import (
    grassfire,
    gaugeclass,
    compcircle,
    pointerRecognition,
    segmentation
)

# Detect gauge with grassfire and rotation
def gaugeDetectGfRot(frame):

    adthresh = segmentation.segment(frame) # Preprocessing and segmentation

    labeled = grassfire.labeling(adthresh) # Labeling of binarized image

    filtered = grassfire.filterObjects(labeled, (np.amin(adthresh.shape)/3)**2) # Filtered labeled image

    center, radius = compcircle.best_circle(filtered) #Find gauge bezel object and compute radius and centroid

    mask = np.zeros(adthresh.shape, dtype=np.uint8) #Define mask image
    cv2.circle(mask, center, radius, 255, -1, 8, 0) # Craete circle in mask

    circle_only = cv2.bitwise_and(adthresh, mask) # overlay the threshholded image with the circular mask
    crop = circle_only[center[1]-radius:center[1]+radius, center[0]-radius:center[0]+radius] # Cropped image only containing the sinside of the found gauge bezel

    labeled_thresh = grassfire.labeling(crop) # Relabel cropped, binarized image
    center = (int(crop.shape[1]/2), int(crop.shape[0]/2)) # Center of gauge bezel in new image.

    return gaugeclass.Gauge(labeled_thresh, center, radius) # Return gauge object with relevant data


# Set up of video to process
cap = cv2.VideoCapture("videos/man7cropclipscale.mp4")
# Distributions used to recognize scale marks and the pointer
scale_mark_dist = np.load('marks_dist.npy') # Scale mark distributions
pointer_dist = np.load('pointer_dist.npy') # Pointer distr

#results
angle = []
marks = []

i = 0 # Current frame number
no_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
print("This many frames in video:",no_frames)

t0 = time.time()
time_stack = [0,0,0,0,0]
i_files = 0
total = 0
none_ = 0
skip_frame = 0
for _ in range(0, skip_frame): # Skip specified number of frames
    _, frame = cap.read()
while(cap.isOpened()):
    # Calculation of estimated time to completion
    time_stack.append(time.time()-t0)
    del time_stack[0]
    print("Estimated time left:",(no_frames-i)*(sum(time_stack)/len(time_stack)/60),"min")
    t0 = time.time()
    
    # Read new image
    _, frame = cap.read() 

    if i == no_frames: # If end of video is reached
        # Save relevant data
        np.save('data/angle', angle)
        print('exitting')
        exit()
    i += 1 # Current no image
    total += 1 # total number of images

    # Filter and binarize the frame, then find the gauge bezel object and label all objects inside the gauge
    g = gaugeDetectGfRot(frame)
    # g is an object that contains:
    # g.getCenterPoint()    Return the gauges centroid relative to the cropped gauge image
    # g.getRadius()         Returns the gauges radius
    # g.getBlobList()       Returns the binarized and labeled image of the inside of the found gauge bezel object

    if g.getblobList() is None: # If it was not possible to find a gauge
        # Append data with zeros! Should be removed from the data after completion.
        angle.append(0)
        marks.append([0])
        none_ +=1 # Number of images that could not be processed.
    else:
        height, width,_ = frame.shape

        if g.getblobList().shape[0] > 50 and g.getblobList().shape[1] > 50 and g.getRadius() > 0 and g.getCenterPoint()[0] > 0 and g.getCenterPoint()[1] > 0: # If the found gauge is not something to small and the centroid is valid
            # Recognize pointer and return its angle.
            # Scale mark angles are stored in mark_angle
            pointer_angle, mark_angle = pointerRecognition.recognition(g.getblobList(),2,g.getCenterPoint(),g.getRadius(),scale_mark_dist,pointer_dist)
            #Append relevant data
            angle.append(pointer_angle)
            marks.append([mark_angle])
        else:
            # Append data with zeros! Should be removed from the data after completion.
            angle.append(0)
            marks.append([0])
            none_ += 1 # Number of images that could not be processed.
    
    #Save relevant data on the fly
    '''
    # Matlab data
    scipy.io.savemat(
        'data/med_test.mat', {
            'b': beta,
            'a': alpha,
            'theta': np.array(angle),
            'marks': marks
        })
    # Numpy data
    np.save('data/angle', angle)
    '''




    print("frame",i)
    print("Succesrate:",100 - none_/total*100,"%")
    #cv2.imshow("img", gauge_frame)
    #if cv2.waitKey(1) & 0xFF == ord('q'):
    #    break
