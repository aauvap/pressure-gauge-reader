﻿# Reading Analogue Gauges Using Image Processing

This is the source code associated with the paper, Reading circular analogue gauges using digital image processing. This code translates videos of circular analouge gauges to a time series. 
The data that is used in the paper can be found at [Kaggle] (http://kaggle.com/juliusgrassme/pressure-gauge-reader-data). 

## Usage

In the file main.py, please specify the video that needs to be processed in line 39. Run the file main.py, and collect the angle data in the data directory. 

## Refference

Please cite the source code with:

@misc{Bitbucketlink,
  AUTHOR =       "J. S. Lauridsen and J. G. Grassmé",
  TITLE =        "Bitbucket Pressure Gauge Reader source code",
  howpublished = "https://bitbucket.org/aauvap/pressure-gauge-reader [\href{https://bitbucket.org/aauvap/pressure-gauge-reader}{Link}]",
  YEAR =         "2019",
}

Citation of article:

@inproceedings{Lauridsen19,
title = "Reading circular analogue gauges using digital image processing",
author = "Lauridsen, {Jakob S.} and Graasm{\'e}, {Julius A. G.} and Malte Pedersen and Jensen, {David Getreuer} and Andersen, {S{\o}ren Holm} and Moeslund, {Thomas B.}",
year = "2019",
language = "English",
booktitle = "Proceedings of the 14th International Conference on Computer Vision Theory and Applications",
}

Citation of the data set used in the article

@misc{Kagglelink,
  AUTHOR =       "J. S. Lauridsen and J. G. Grassmé",
  TITLE =        "Kaggle Pressure Gauge Reader data set",
  howpublished = "http://kaggle.com/juliusgrassme/pressure-gauge-reader-data [\href{https://kaggle.com/juliusgrassme/pressure-gauge-reader-data}{Link}]",
  YEAR =         "2019",
}

## License

MIT License

Copyright (c) 2019 Lauridsen J. S., Grassmé J., Damsgaard S. B. 

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.



