import numpy as np

def pca(X):
    """
    Takes a 2 x N matrix with x,y points and calculates the direction of the point cloud
    [[x1, x2, x3, x4],
     [y1, y2, y3, y4]]
    """
    #Simple error handling
    if X.shape[0] != 2:
        print('Error: Matrix X was not two dimensional')
        return -1

    N = X.shape[1] # N data points in matrix X each being x_i

    m = np.mean(X,axis = 1) # The mean of all x_i
    z = np.apply_along_axis(np.subtract,0,X,m) # Subtract mean from each x_i

    S = np.dot(z,z.T) /  N # Calculate covariance matrix

    d, V = np.linalg.eig(S) # Find eigen values and eigenvectors
    idx = np.argmax(d) # Find largest eigenvalue
    return V[:,idx] # Return eigenvector asociated with largest eigenvalue

def pcaExstract(img):
    idx = np.nonzero(img) # Return indices with nonzero elements
    return np.array([idx[1],idx[0]]) # Create matrix with the corret dimensions


