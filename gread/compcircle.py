import numpy as np
import cv2

from gread import (
    grassfire,
    lsc
    )

def compare_to_self(obj, x, y):
    # Computes the circle score of an object

    reference = obj.copy() # Definition of refrence object that is to be compared to rotated versions
    result = np.zeros(obj.shape, dtype=np.uint16) # Result image

    for theta in range(0, 360, 10): # For each angle: theta = 0,10,...,350
        M = cv2.getRotationMatrix2D((x, y), theta, 1) # Rotation relative to bounding box
        rotated = cv2.warpAffine(obj, M, (obj.shape[1], obj.shape[0])) # Rotated version of object

        result = result + cv2.bitwise_and(reference, rotated) # Updated score with the comparison of the refference and rotation

    return np.sum(result) # Return score

def best_circle(blobs):
    bestCircle = 0 # Best score
    center = None # Center definition
    radius = 0 # #Radius definition
    bestShape = None #Best object label (Dedug purpose)

    for nr in range(1, grassfire.getLabelCount(blobs)+1): # For each blob
        blob = grassfire.getLabel(blobs, nr) # Isolate object


        x, y, w, h = cv2.boundingRect(blob) #Bounding box of object
        # Center of bounding box (Used for rotation in score calculation)
        x = int(x+w/2)
        y = int(y+h/2)

        vote = compare_to_self(blob, x, y) # Calculate score of obejct
        if vote > bestCircle: # If score is highest score
            bestCircle = vote # Update best score
            c = lsc.lest_sqr_circ(blob) # Compute centroid an radius of object
            center = c[0]
            radius = c[1]
            bestShape = nr

    return center, radius # Return centroid and radius of best object
