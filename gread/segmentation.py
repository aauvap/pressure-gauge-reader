import numpy as np
import cv2

def segment(img):
    # Function the preprocesses and segments the input image.
    # First we apply bilateral filtering thrice 
    img = cv2.bilateralFilter(img,30,75,75)
    img = cv2.bilateralFilter(img,30,75,75)
    img = cv2.bilateralFilter(img,30,75,75)

    imgResult = img.astype(np.float32)*2
    imgResult = cv2.convertScaleAbs(imgResult)
    imgqqq = cv2.cvtColor(imgResult,cv2.COLOR_RGB2GRAY) # Grayscale conversion
    #Apply adaptive threshold
    th = cv2.adaptiveThreshold(imgqqq, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV, 7, 2 )  
    #copyimg = th 
    #th = cv2.medianBlur(copyimg,3)
    return th # Return threshholded image

	