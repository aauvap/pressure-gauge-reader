import numpy as np
import cv2

def labeling(input):
    #Labels a binary image

    img = input.copy()
    output = np.zeros(img.shape, dtype=np.int16) # Output definition
    _, output = cv2.connectedComponents(input, connectivity=4, ltype=cv2.CV_16U) # Grassfire using cv2 implementation four way connection
    return output

def getLabelCount(input):
    return np.amax(input)

def getLabel(input, label):
    tmp = np.where(input != label, 0, input) # Remove all objects that is not the label
    return np.where(tmp == label, 255, tmp).astype(np.uint8) # scale isolated image with 255

def removeLabel(input, label):
    tmp = np.where(input == label, 0, input) # Set any entry in input that is equal to label to 0.
    return np.where(input > label, tmp-1, tmp) # Subtract 1 from all entris greater than label.

def filterObjects(blobs, size):
    #Removes small objects from threholded image
    labels = getLabelCount(blobs)+1 # No. labels
    no = 1 # Current label/object
    while no < labels:

        shape = getLabel(blobs, no) #Isolate the current object
        x, y, w, h = cv2.boundingRect(shape) # Calculate bounding box

        if w*h < size:  #If area is too small
            blobs = removeLabel(blobs, no) # Remove that object
        else:
            no = no + 1 # Next object
        labels = getLabelCount(blobs)+1 # Update label maximum
    return blobs # Return filtered image

