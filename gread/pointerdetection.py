import numpy as np
import cv2

from gread import (
    pca
) 

def get_angle(p0, p1=np.array([0,0]), p2=None):
    ''' compute angle (in radians) for p0p1p2 corner
    Inputs:
        p0,p1,p2 - points in the form of [x,y]
    '''
    if p2 is None:
        p2 = p1 + np.array([1, 0])
    v0 = np.array(p0) - np.array(p1)
    v1 = np.array(p2) - np.array(p1)

    angle = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1)) # Angle calculation
    return angle # In radians

def angle_in_between(a,b):
  arccosInput = np.dot(a,b)/np.linalg.norm(a)/np.linalg.norm(b)
  arccosInput = 1.0 if arccosInput > 1.0 else arccosInput
  arccosInput = -1.0 if arccosInput < -1.0 else arccosInput
  return np.arccos(arccosInput)

def find_angle_pointer(binary_img, the_center_point, radius):

    crop_radius = radius/5 # Radius for cropping the pointer

    x,y,w,h = cv2.boundingRect(binary_img) # Bounding box of the pointer inside the input image
    point_vec = ( int(x+w/2)-the_center_point[0], int(y+h/2)-the_center_point[1] ) # Vector from center of gauge to bounding box center of pointer object

    circlemask = np.zeros(binary_img.shape, np.uint8) # Empty mask for cropping the pointer in the binary image
    cv2.circle(circlemask, the_center_point, int(crop_radius), 255, thickness=-1, lineType=8, shift=0) # Create circle in mask that has the crop radius.

    masked_img_out = cv2.bitwise_and(binary_img, binary_img, mask = circlemask) # Mask binary input image

    if np.count_nonzero(masked_img_out) < 2: # If there are no pixels left
        return 0 # No recognized pointer
    else:
        prepped = pca.pcaExstract(masked_img_out) # Prepare data for PCA: from image to (x,y) points
        nVec = pca.pca(prepped) # PCA of points 
        nVecU = nVec / np.linalg.norm(nVec) # To unit vector
        #if angle from nVecU to point is smaller then -nVecU to point
        if angle_in_between(nVecU, point_vec) < np.pi -angle_in_between(point_vec , nVecU):
            nVecU = nVecU
        else:
            nVecU = -nVecU # Orient the vector correctly

        angle = -get_angle(nVecU) # Angle of vector
        
        # Make sure angle is between 0 and 2 pi
        if angle < 0:
            angle = angle+2*np.pi

        if angle > np.pi/2:
            angle = angle-2*np.pi

        return angle # Return angle
