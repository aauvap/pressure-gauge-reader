import numpy as np

def lest_sqr_circ(blob):
    coords = np.nonzero(blob) # Get the coordinates of all entries that are not 0. ex: (x_0,y_0)

    x = coords[1] # First dimension of coords
    y = coords[0] # Second dimensions of coords

    N = np.size(x) # Number of points/non-zero entries
    if N == 0: # If empty object
        return ((0 ,0 ), 1)

    xhat = (1/N)*sum(x) # Sample mean of x
    yhat = (1/N)*sum(y) # Sample mean of y
    # coordinate transform
    u = x - xhat
    v = y - yhat
    
    #Calculate relevant sums. Please reffer to the paper for further explanation.
    Suu = np.sum(np.power(u, 2))
    Suv = np.sum(u * v)
    Svv = np.sum(np.power(v, 2))
    Suuu = np.sum(np.power(u, 3))
    Svvv = np.sum(np.power(v, 3))
    Suvv = np.sum(u * np.power(v, 2))
    Svuu = np.sum(v * np.power(u, 2))

    p1 = np.array([[Suu, Suv], [Suv, Svv]])
    p2 = np.array([(1/2)*(Suuu+Suvv), (1/2)*(Svvv+Svuu)])

    uv = np.linalg.lstsq(p1, p2, rcond=None) # Least squares

    #Coordinate transform of center
    xc = uv[0][0] + xhat
    yc = uv[0][1] + yhat

    # Calculate circle radius
    r = np.sqrt( np.power(uv[0][0], 2) + np.power(uv[0][1], 2) + (Suu+Svv)/N )

    return ((int(np.rint(xc)), int(np.rint(yc))), int(np.rint(r))) # Return center and radius
