import numpy as np

class Gauge(object):
    def __init__(self, blist, center, rad):
        self.blobList = blist
        self.centerPoint = center
        self.radius = rad
    
    def getblobList(self):
        return self.blobList

    def getCenterPoint(self):
        return self.centerPoint
    
    def getRadius(self):
        return self.radius



