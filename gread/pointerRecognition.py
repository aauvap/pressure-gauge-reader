import numpy as np
import cv2
import scipy.stats as ss
from gread import (pointerdetection)
 

def malahobis(X,u,cov,label):
    # Calculation of the malahobis distance of points to a given distribution
    invSig = np.linalg.inv(cov) # Inversion of the covariance matrix
    d = [] # #Distances
    for p in X: # for each point
        tmp = np.matmul((p - u), invSig)
        d.append(np.sqrt(np.matmul(tmp, (p - u).T))) # Append distance
    #print(sorted(d))
    return label[np.argmin(d)] # Return label of the object that has the shortest distance


def sb(a):
    # Calculation of the bounding box of an isolated object in an image.
    r = a.any(1)
    if r.any(): # If any non-zero entries in input
        m, n = a.shape 
        c = a.any(0)
        out = [[r.argmax(), m - r[::-1].argmax() - 1],
               [c.argmax(), n - c[::-1].argmax() - 1]]
    else:
        out = np.empty((0, 0), dtype=bool)
    return out


def deter(x, mu, cov, pi, prior):
    p_true = ss.multivariate_normal.pdf(x, np.array(mu[0]), cov[0]) * prior[0] # Likelyhood that objects are from the scale mark distribution
    p_false = (pi[0] * ss.multivariate_normal.pdf(x, mu[1], cov[1]) +
               pi[1] * ss.multivariate_normal.pdf(x, mu[2], cov[2])) * prior[1] # Likelyhood that objects are from noise distributions
    return p_true > p_false # Return Trues for scale mark objects


def recognition(img, BLOB_thresh,center,radius,mark_dist,pointer_dist):

    center = center[::-1] # center format conversion

    labels = img
    # BLOB Segmentation
    n_blobs = np.max(labels)  # Number of found BLOBS
    #Initiazation of features and blob boxes
    dist = []  # Distance from BLOB to center
    bbr = []  # bbrness of the BLOB
    area = []  # Number og pixels in BLOB

    angle = []  # Angle of BLOB
    labelid = [] # Label id's
    for i in range(n_blobs):
        im = np.copy(
            labels)  # Create copy of labels as we begin messing with values
        im[im != i + 1] = 0  # Remove all but one BLOB
        bob = sb(im)  # Find a boundng box for current BLOB
        if bob[0][1] - bob[0][0] > BLOB_thresh and bob[1][1] - bob[1][0] > BLOB_thresh:
            labelid.append(i) # Append current label

            #Distance calculation
            p = [sum(k) / 2 for k in bob]  # Find center of boundingbox / center of BLOB
            pc = [j - k for j, k in zip(p, center)]  # Vector from gauge center to BLOB center
            dist.append(np.sqrt(sum([k**2 for k in pc])) / radius)  # length of ventor stored as feature Normalized with gauge radius

            #Angles for scale possible marks
            theta = np.arctan2(pc[0],pc[1])
            angle.append(theta)

            # Crop image to only contain current object
            mark = im[bob[0][0]:bob[0][1], bob[1][0]:bob[1][1]]  # Create image with only current BLOB in
            mark = (mark/np.max(mark)*255).astype(np.uint8) # Scale image

            # Calculation of object mass
            area.append(np.log(np.count_nonzero(mark)))  # Count nonzero elements in BLOB and transform with natural logarithm

            rows, cols = mark.shape  # Shape of BLOB image

            M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 180 / np.pi * theta,1)  # Calculate rotation matrix
            rot = cv2.warpAffine(mark, M, (int(np.sqrt(cols**2 + rows**2)), int(np.sqrt(cols**2 + rows**2))))  # Rotate image

            rec = sb(rot)  # Find bounding box of rotated image
            if np.count_nonzero(rot) > 0:
                # Calculations of features
                r = rec[0][1] - rec[0][0] + 1  # Number of rows for compact-BLOB
                c = rec[1][1] - rec[1][0] + 1  # Number of columns for compact-BLOB
                if r > 0 and c > 0:  # Also now we have compactangles with zero length
                    bbr.append(c / r)  # Store ratio of sides as feature
                else:
                    bbr.append(0) # If something is wrong
            else:
                bbr.append(0)  # If something is wrong

    # Creating relevant data matrixes for recognition
    data_mat_marks = np.array([[i1, i2] for i1,i2 in zip(bbr, dist)]) # Data matrix for scale mark recognition. Feature Bounding box ratio and distance between gauge senter and bounding box center
    data_mat_pointer = np.array([[i1, i2] for i1, i2 in zip(dist,area)]) # Data matrix for pointer recognition. Mass of objects and distance again.

    # Object recognition
    is_mark = deter(data_mat_marks, mark_dist[0], mark_dist[1], mark_dist[2],mark_dist[3]) # Determine which objects are most likely scale marks
    pointer_id = malahobis(data_mat_pointer,pointer_dist[0][0],pointer_dist[1][0],labelid) # Determine which object is the pointer object.
    
    # Pointer angle estimation
    pimg = np.zeros(labels.shape, dtype=np.uint8) # Create empty image
    pimg[labels == pointer_id + 1] = 255 # Put in the pointer object
    pointer_angle = pointerdetection.find_angle_pointer(pimg, center, radius) # Estimate the angle of the pointer (radians)

    mark_angle = np.array(angle)[is_mark] # Angles of recognized marks

    print('Angle is', pointer_angle)

    return pointer_angle, mark_angle
 